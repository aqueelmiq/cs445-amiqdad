package com.project.cs445.dao;

import com.project.cs445.model.Food;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import static org.junit.Assert.*;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */
public class Sql2oFoodDaoTest {

    private Sql2oFoodDao dao;
    private Connection con;

    @Before
    public void setUp() throws Exception {
        String conString = "jdbc:h2:mem:testing;INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString,"","");
        dao = new Sql2oFoodDao(sql2o);
        //Keep connection open for testing
        con = sql2o.open();
    }

    @After
    public void tearDown() throws Exception {
        con.close();
    }

    @Test
    public void addFoodSetsID() throws Exception {
        Food food = sampleFood();
        int originalId = food.getId();

        dao.addFood(food);

        assertNotEquals(originalId, food.getId());

    }

    @Test
    public void findAllReturnsOne() throws Exception {
        Food food = sampleFood();
        int originalId = food.getId();

        dao.addFood(food);

        assertEquals(1, dao.findAll().size());
    }

    @Test
    public void findAllReturnsNone() throws Exception {
        assertEquals(0, dao.findAll().size());
    }

    @Test
    public void findFoodByIdFindsFood() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);
        int id = food.getId();

        assertEquals(food.toString(), dao.findById(id).toString());

    }

    @Test
    public void findFoodByNameFindsFood() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);
        String name = food.getName();

        assertEquals(food.toString(), dao.findByName(name).toString());

    }

    @Test
    public void findFoodByNameNotFindsFood() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);

        assertNotEquals(food, dao.findByName("blah"));

    }

    @Test
    public void findFoodByTypeFindsFood() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);
        String name = food.getCategories();

        assertEquals(1, dao.findByType(name).size());

    }

    @Test
    public void findFoodByTypeNotFindsFood() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);

        assertNotEquals(food, dao.findByName("blah"));

    }

    @Test
    public void deleteByIDWorksNonEmpty() throws Exception {

        Food food = sampleFood();

        dao.addFood(food);
        int originalId = food.getId();

        dao.deleteById(originalId);

        assertNotEquals(1, dao.findAll().size());

    }

    @Test
    public void deleteByNameWorksEmpty() throws Exception {


        int returns = dao.deleteById(12);
        assertEquals(0, returns);

    }

    @Test
    public void deleteByNameWorksMultiple() throws Exception {


        Food food = sampleFood();
        Food food2 = sampleFood2();
        dao.addFood(food);
        dao.addFood(food2);
        int returns = dao.deleteById(food2.getId());
        assertEquals(1, returns);

    }

   // @Test
    public void updatePriceByNameModifies() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updatePriceByName(food.getName(), 5);
        assertEquals(5, dao.findByName(food.getName()).getPrice_per_person());

    }

    @Test
    public void updateQTYByNameModifies() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updateQTYByName(food.getName(), 500);
        assertEquals(500, dao.findByName(food.getName()).getMinimum_order());

    }

    @Test
    public void updatePriceByIDModifies() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updatePriceById(food.getId(), 5);
        assertEquals(5, (int)dao.findByName(food.getName()).getPrice_per_person());

    }

    @Test
    public void updateQTYByIDModifies() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updateQTYById(food.getId(), 500);
        assertEquals(500, dao.findByName(food.getName()).getMinimum_order());

    }

    @Test
    public void updatePriceByNameOnEmpty() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updatePriceByName("blah", 5000);
        assertNotEquals(5000, dao.findByName(food.getName()).getPrice_per_person());

    }

    @Test
    public void updateQTYByNameOnEmpty() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updateQTYByName("blah", 5000);
        assertNotEquals(5000, dao.findByName(food.getName()).getMinimum_order());

    }

    @Test
    public void updatePriceByIDOnEmpty() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updatePriceByName("blah", 5000);
        assertNotEquals(5000, dao.findByName(food.getName()).getPrice_per_person());

    }

    @Test
    public void updateQTYByIDOnEmpty() throws Exception {


        Food food = sampleFood();
        dao.addFood(food);
        dao.updateQTYByName("blah", 5000);
        assertNotEquals(5000, dao.findByName(food.getName()).getMinimum_order());

    }

    private Food sampleFood() {
        return new Food("Chicken Soup", "Non-Veg", 3, 100);
    }

    private Food sampleFood2() {
        return new Food("Schezwan Noodles", "Veg", 4, 50);
    }

}