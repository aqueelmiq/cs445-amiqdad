package com.project.cs445.dao;

import com.project.cs445.model.Food;
import com.project.cs445.model.OrderItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by aqueelmiqdad on 4/15/16.
 */
public class Sql2oItemDaotest {

    private Sql2oItemDao dao;
    private Sql2oFoodDao daoF;
    private Connection con;

    @Before
    public void setUp() throws Exception {
        String conString = "jdbc:h2:mem:testing;INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString,"","");
        dao = new Sql2oItemDao(sql2o);
        daoF = new Sql2oFoodDao(sql2o);
        //Keep connection open for testing
        con = sql2o.open();
    }

    @After
    public void tearDown() throws Exception {
        con.close();
    }

    @Test
    public void addSetsId() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = new OrderItem(food.getId(), 100, food.getPrice_per_person());
        int originalId = item.getId();
        dao.add(item);

        assertNotEquals(originalId, item.getId());

    }

    @Test
    public void findAllReturnsOne() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = new OrderItem(food.getId(), 100, food.getPrice_per_person());
        int originalId = item.getId();
        dao.add(item);

        assertEquals(1, dao.findAll().size());
    }

    @Test
    public void findAllReturnsNone() throws Exception {
        assertEquals(0, dao.findAll().size());
    }

    @Test
    public void findByIdReturnsOne() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = new OrderItem(food.getId(), 100, food.getPrice_per_person());
        dao.add(item);

        assertEquals(item.toString(), dao.findById(item.getId()).toString());
    }

    private Food sampleFood() {
        return new Food("Chicken Breast", "non-veg", 4, 400);
    }

    @Test
    public void findByIdReturnsNone() throws Exception {

        assertNull(dao.findById(1));
    }



}
