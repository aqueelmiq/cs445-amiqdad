package com.project.cs445.dao;

import com.project.cs445.model.Customer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import static org.junit.Assert.*;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */
public class Sql2oCustomerDaoTest {

    private Sql2oCustomerDao dao;
    private Connection con;

    @Before
    public void setUp() throws Exception {
        String conString = "jdbc:h2:mem:testing;INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString,"","");
        dao = new Sql2oCustomerDao(sql2o);
        //Keep connection open for testing
        con = sql2o.open();
    }

    @After
    public void tearDown() throws Exception {
        con.close();
    }

    private Customer sampleCustomer() {
        return new Customer("Test", "Last", "3125369986", "test@test.com");
    }

    @Test
    public void addSetsID() throws Exception {
        Customer customer = sampleCustomer();
        int origId = customer.getId();
        dao.add(customer);

        assertEquals(1, dao.findAll().size());

    }

    @Test
    public void findAllReturnsOne() throws Exception {
        Customer customer = sampleCustomer();
        int originalId = customer.getId();

        dao.add(customer);

        assertEquals(1, dao.findAll().size());
    }



    @Test
    public void findAllReturnsNone() throws Exception {
        assertEquals(0, dao.findAll().size());
    }


    @Test
    public void findCustomerByNameFindsCustomer() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertEquals(customer.toString(), dao.findByName(customer.getlName()).get(0).toString());

    }



    @Test
    public void findCustomerByNameNotFindsCustomer() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertNotEquals(customer, dao.findByName("blah"));

    }


    @Test
    public void findCustomerByEmail() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertEquals(customer.toString(), dao.findByEmail(customer.getEmail()).toString());

    }

    @Test
    public void findCustomerByEmailNotFinds() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertNull(dao.findByEmail("blah"));

    }


    @Test
    public void findCustomerByPhone() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertEquals(customer.toString(), dao.findByPhone(customer.getPhone()).toString());

    }

    @Test
    public void findCustomerByPhoneNotFinds() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertNull(dao.findByPhone("blah"));

    }

    @Test
    public void findCustomerByID() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertNotNull(dao.findById(customer.getId()));

    }

    @Test
    public void findCustomerByIDNull() throws Exception {

        Customer customer = sampleCustomer();

        dao.add(customer);

        assertNull(dao.findById(customer.getId()+1));

    }


}