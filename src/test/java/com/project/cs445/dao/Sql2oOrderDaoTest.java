package com.project.cs445.dao;

import com.project.cs445.model.Customer;
import com.project.cs445.model.Food;
import com.project.cs445.model.Order;
import com.project.cs445.model.OrderItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by aqueelmiqdad on 4/19/16.
 */
public class Sql2oOrderDaoTest {

    private Sql2oOrderDao dao;
    private Sql2oFoodDao daoF;
    private Sql2oItemDao daoI;
    private Sql2oCustomerDao daoC;
    private Connection con;

    @Before
    public void setUp() throws Exception {
        String conString = "jdbc:h2:mem:testing;INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString,"","");
        dao = new Sql2oOrderDao(sql2o);
        daoF = new Sql2oFoodDao(sql2o);
        daoI = new Sql2oItemDao(sql2o);
        daoC = new Sql2oCustomerDao(sql2o);
        //Keep connection open for testing
        con = sql2o.open();
    }

    @After
    public void tearDown() throws Exception {
        con.close();
    }

    @Test
    public void addOrderSetsID() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertNotEquals(originalId, order.getId());

    }

    @Test
    public void findAllOrdersReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(1, dao.findAll().size());

    }

    @Test
    public void findAllOrdersReturnsNone() throws Exception {

        assertEquals(0, dao.findAll().size());

    }

    @Test
    public void findOrderByCustomerEmailReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(1, dao.findByCustomerEmail(cust.getEmail()).size());

    }

    @Test
    public void findOrderByCustomerEmailReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByCustomerEmail("blah").size());

    }

    @Test
    public void findOrderByOrderIdReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertNotNull(dao.findByOrderId(order.getOrderNum()));

    }

    @Test
    public void findOrderByOrderIdReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertNull(dao.findByOrderId(order.getOrderNum()+1));

    }

    @Test
    public void findOrderByExactOrderDateReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Date ex = order.getOrderDate();

        ex = formatter.parse(formatter.format(ex));
        assertEquals(1, dao.findByExactDate(ex).size());

    }

    @Test
    public void findOrderByExactOrderDateReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByExactDate(new Date(2013, 2, 22)).size());

    }

    @Test
    public void findOrderByOrderNum() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);
        System.out.println(dao.findOrder(100));
        assertEquals(1, dao.findOrder(100).size());

    }

    @Test
    public void findOrderByOrderDateReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Date ex = order.getOrderDate();

        ex = formatter.parse(formatter.format(ex));
        assertEquals(1, dao.findByDate(ex).size());

    }

    @Test
    public void findOrderByOrderDateReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByDate(new Date(2013, 2, 22)).size());

    }

    @Test
    public void findOrderByDeliverDateEndReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByDeliveryDate(new Date(2013, 2, 22), new Date(2019, 2, 23)).size());

    }

    @Test
    public void findOrderByOrderDateEndReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByDate(new Date(2013, 2, 22), new Date(2019, 2, 23)).size());

    }

    @Test
    public void findOrderByDeliverDateReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByDeliveryDate(new Date(new Date().getTime() + 10000*24*60*60)).size());

    }

    @Test
    public void findOrderByDeliverDateReturns() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(1, dao.findByDeliveryDate(new Date()).size());

    }


    @Test
    public void findOrderByOrderDateEndReturnsNone() throws Exception {
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);

        assertEquals(0, dao.findByDate(new Date(2013, 2, 22), new Date(2013, 2, 23)).size());

    }

    @Test
    public void cancelOrderCancels() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);
        dao.cancelOrder(order.getOrderNum());

        assertEquals("cancelled", dao.findByOrderId(order.getOrderNum()).getStatus());

    }

    @Test
    public void cancelOrderCancelsCorrect() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);
        dao.cancelOrder(order.getOrderNum()+1);

        assertNotEquals("cancelled", dao.findByOrderId(order.getOrderNum()).getStatus());

    }

    @Test
    public void deliverOrderDelivers() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);
        dao.deliverOrder(order.getOrderNum());

        assertEquals("delivered", dao.findByOrderId(order.getOrderNum()).getStatus());

    }

    @Test
    public void deliverOrderDeliversCorrect() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);
        dao.deliverOrder(order.getOrderNum()+1);

        assertNotEquals("delivered", dao.findByOrderId(order.getOrderNum()).getStatus());

    }

    @Test
    public void findStatusReturnsCorrect() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);

        dao.deliverOrder(order.getOrderNum());

        assertEquals(1, dao.findOrderByStatus("delivered").size());

    }

    @Test
    public void findStatusReturnsNone() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);

        assertEquals(0, dao.findOrderByStatus("cancelled").size());

    }

    @Test
    public void orderDaoSetAmount() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        dao.add(order);
        dao.add(order);

        dao.setAmount(150, order.getOrderNum());

        assertEquals(150, (int)dao.findOrder(order.getOrderNum()).get(1).getAmount());
    }




    private Order sampleOrder(OrderItem item, Customer cust) {
        return new Order(100, cust.getEmail(), 0, item.getId(), "baby", 0.75, "open", new Date(), new Date(((new Date()).getTime()) + 1000*24*60*60), "blah blah blah");
    }

    private OrderItem sampleItem(Food food) {
        return new OrderItem(food.getId(), 100, food.getPrice_per_person());
    }

    private Customer sampleCustomer() {
        return new Customer("Mike", "Jordan", "3215676780", "mike.jordan@gmail.com");
    }

    private Food sampleFood() {
        return new Food("Chicken Soup", "Non-Veg", 1.5, 35);
    }
}
