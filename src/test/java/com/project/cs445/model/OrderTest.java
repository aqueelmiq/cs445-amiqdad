package com.project.cs445.model;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class OrderTest {

    @Test
    public void calcSurcharge() throws Exception {
        Food food = sampleFood();
        OrderItem item = sampleItem(food);
        Customer cust = sampleCustomer();
        Order order = sampleOrder(item, cust);
        Calendar c = Calendar.getInstance();
        c.set(c.DAY_OF_WEEK, c.SUNDAY);
        DateFormat df = new SimpleDateFormat("EEE dd/MM/yyyy");
        order.calcSurcharge(df.parse(df.format(c.getTime())));
        assertNotEquals(order.getSurcharge(), 0);

    }

    @Test
    public void calcSurchargeWeekday() throws Exception {
        Food food = sampleFood();
        OrderItem item = sampleItem(food);
        Customer cust = sampleCustomer();
        Order order = sampleOrder(item, cust);
        Calendar c = Calendar.getInstance();
        c.set(c.DAY_OF_WEEK, c.MONDAY);
        DateFormat df = new SimpleDateFormat("EEE dd/MM/yyyy");
        order.calcSurcharge(df.parse(df.format(c.getTime())));
        assertNotEquals(order.getSurcharge(), 0.75);

    }

    private Order sampleOrder(OrderItem item, Customer cust) {
        return new Order(100, cust.getEmail(), 0, item.getId(), "baby", 0.75, "open", new Date(), new Date(((new Date()).getTime()) + 1000*24*60*60), "blah blah blah");
    }
    private OrderItem sampleItem(Food food) {
        return new OrderItem(food.getId(), 100, food.getPrice_per_person());
    }

    private Customer sampleCustomer() {
        return new Customer("Mike", "Jordan", "3215676780", "mike.jordan@gmail.com");
    }

    private Food sampleFood() {
        return new Food("Chicken Soup", "Non-Veg", 1.5, 35);
    }

}