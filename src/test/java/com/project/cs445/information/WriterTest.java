package com.project.cs445.information;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class WriterTest {

    private Writer wt;

    @Before
    public void setWriter() throws IOException {
        wt = new Writer("sampleOrder.txt", "sampleSurcharge.txt");
        wt.setOrderNum(100);
        wt.setSurcharge(0);
    }
    
    @Test
    public void getOrderNum() throws Exception {
        int orderNum = wt.getOrderNum();
        assertEquals(100, orderNum);
    }

    @Test
    public void setOrderNum() throws Exception {
        wt.setOrderNum(200);
        int orderNum = wt.getOrderNum();
        assertNotEquals(100, orderNum);
    }

    @Test
    public void getSurcharge() throws Exception {
        int sur = (int) wt.getSurcharge();
        assertEquals(0, sur);
    }

    @Test
    public void setSurcharge() throws Exception {
        wt.setSurcharge(50);
        int sur = (int) wt.getSurcharge();
        assertNotEquals(0, sur);

    }

}