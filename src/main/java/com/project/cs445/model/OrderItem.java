package com.project.cs445.model;

/**
 * Created by aqueelmiqdad on 4/12/16.
 */

public class OrderItem {

    private int foodid;
    private int qty;
    private double price;
    private int id;

    public OrderItem(int foodid, int qty, double price) {
        this.foodid = foodid;
        this.qty = qty;
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "foodid=" + foodid +
                ", qty=" + qty +
                ", price=" + price +
                ", id=" + id +
                '}';
    }

    public int getFoodid() {
        return foodid;
    }

    public void setFoodid(int foodid) {
        this.foodid = foodid;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
