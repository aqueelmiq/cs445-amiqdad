package com.project.cs445.model; /**
 * Created by aqueelmiqdad on 4/10/16.
 */

import java.util.Calendar;
import java.util.Date;

public class Order {

    private int id;
    private int orderNum;
    private String ordered_by;
    private double amount;
    private int itemid;
    private String special;
    private double surcharge;
    private String status;
    private Date orderDate;
    private Date deliveryDate;
    private String deliveryAddress;

    public Order(int orderNum, String ordered_by, double amount, int itemid, String special, double surcharge, String status, Date orderDate, Date deliveryDate, String deliveryAddress) {
        this.orderNum = orderNum;
        this.ordered_by = ordered_by;
        this.itemid = itemid;
        this.special = special;
        this.surcharge = surcharge;
        this.status = status;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.deliveryAddress = deliveryAddress;
        this.amount = amount;
        calcSurcharge(deliveryDate);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getOrdered_by() {
        return ordered_by;
    }

    public void setOrdered_by(String ordered_by) {
        this.ordered_by = ordered_by;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getStatus() {
        return status;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(double surcharge) {
        this.surcharge = surcharge;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNum=" + orderNum +
                ", ordered_by='" + ordered_by + '\'' +
                ", itemid=" + itemid +
                ", special='" + special + '\'' +
                ", surcharge=" + surcharge +
                ", status='" + status + '\'' +
                ", orderDate=" + orderDate +
                ", deliveryDate=" + deliveryDate +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                '}';
    }

    public void calcSurcharge(Date deliveryDate) {

        Calendar c = Calendar.getInstance();
        c.setTime(deliveryDate);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if(dayOfWeek != 1 && dayOfWeek != 7)
            setSurcharge(0);
    }
}
