package com.project.cs445.model;

import com.project.cs445.information.FoodStruct;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */

public class Food {

    private int id;
    private String name;
    private String categories;
    private double price_per_person;
    private int minimum_order;

    public Food(String name, String categories, double price_per_person, int minimum) {
        this.name = name;
        this.minimum_order = minimum;
        this.price_per_person = price_per_person;
        this.categories = categories;
    }

    public Food(FoodStruct food) {
        this.name = food.getName();
        this.minimum_order = food.getMinimum_Order();
        this.price_per_person = food.getPrice_per_person();
        this.categories = food.getCategories();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategories() {
        return categories;
    }

    public double getPrice_per_person() {
        return price_per_person;
    }

    public int getMinimum_order() {
        return minimum_order;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categories='" + categories + '\'' +
                ", price_per_person=" + price_per_person +
                ", minimum_order=" + minimum_order +
                '}';
    }
}
