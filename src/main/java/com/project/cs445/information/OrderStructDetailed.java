package com.project.cs445.information;

import com.project.cs445.model.Customer;
import com.project.cs445.model.Order;
import com.project.cs445.model.OrderItem;

import java.util.Date;
import java.util.List;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class OrderStructDetailed {


    private Date delivery_date;
    private String delivery_address;
    private String status;
    private double amount;
    private double surcharge;
    private CustomerInfo personal_info;
    private String note;
    private List<itemStructDetail> order_detail;

    public OrderStructDetailed(Order order, Customer customer, List<itemStructDetail> order_detail) {
        this.delivery_address = order.getDeliveryAddress();
        this.delivery_date = order.getDeliveryDate();
        this.status = order.getStatus();
        this.amount = order.getAmount();
        this.surcharge = order.getSurcharge();
        this.personal_info = new CustomerInfo(customer);
        this.note = order.getSpecial();
        this.order_detail = order_detail;
    }

    public OrderStructDetailed() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public double getSurcharge() {
        return surcharge;
    }
}
