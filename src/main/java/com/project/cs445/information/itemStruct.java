package com.project.cs445.information;

import com.project.cs445.model.OrderItem;

/**
 * Created by aqueelmiqdad on 4/23/16.
 */
public class itemStruct {

    private int id;
    private int count;

    public itemStruct(OrderItem item) {
        this.id = item.getId();
        this.count = item.getQty();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "itemStruct{" +
                "id=" + id +
                ", count=" + count +
                '}';
    }
}
