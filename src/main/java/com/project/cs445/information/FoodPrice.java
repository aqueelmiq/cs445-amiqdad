package com.project.cs445.information;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class FoodPrice {

    private int id;
    private double price_per_person;

    public FoodPrice(int id, double price_per_person) {
        this.id = id;
        this.price_per_person = price_per_person;
    }

    public int getId() {
        return id;
    }

    public double getPrice_per_person() {
        return price_per_person;
    }
}
