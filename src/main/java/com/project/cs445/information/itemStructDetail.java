package com.project.cs445.information;

import com.project.cs445.model.OrderItem;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class itemStructDetail extends itemStruct {

    private String name;

    public itemStructDetail(OrderItem item, String name) {
        super(item);
        this.name = name;
    }
}
