package com.project.cs445.information;

/**
 * Created by aqueelmiqdad on 4/25/16.
 */
public class Category {

    private String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "name:" + " = " + name;
    }
}
