package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.Customer;

import java.util.List;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */

public class Sql2oCustomerDao implements CustomerDao {

    private final Sql2o sql2o;

    public Sql2oCustomerDao(Sql2o sql2o) {

        this.sql2o = sql2o;
    }

    public void add(Customer customer) throws DaoException {
        String sql = "INSERT INTO CUSTOMERS (fName, lName, phone, email) VALUES (:fName, :lName, :phone, :email)";
        try( Connection con = sql2o.open() ) {

            int id = (int) con.createQuery(sql)
                    .bind(customer)
                    .executeUpdate()
                    .getKey();

            customer.setId(id);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Insert screwd up");
        }
    }

    public List<Customer> findAll() throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM CUSTOMERS";
            return con.createQuery(sql)
                    .executeAndFetch(Customer.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return All Customers Failed");
        }
    }

    public List<Customer> findByName(String name) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM CUSTOMERS WHERE lName = :lName";
            return con.createQuery(sql)
                    .addParameter("lName", name)
                    .executeAndFetch(Customer.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Customer By Name Failed");
        }
    }

    public Customer findByPhone(String phone) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM CUSTOMERS WHERE phone = :phone";
            return con.createQuery(sql)
                    .addParameter("phone", phone)
                    .executeAndFetchFirst(Customer.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Customer By Phone Failed");
        }
    }

    @Override
    public Customer findById(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM CUSTOMERS WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .executeAndFetchFirst(Customer.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Customer By Phone Failed");
        }
    }

    public Customer findByEmail(String email) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM CUSTOMERS WHERE email = :email";
            return con.createQuery(sql)
                    .addParameter("email", email)
                    .executeAndFetchFirst(Customer.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Customer By Email Failed");
        }
    }

}
