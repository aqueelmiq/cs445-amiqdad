package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.*;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */
public interface FoodDao {

    void addFood(Food food) throws DaoException;

    List<Food> findAll() throws DaoException;
    Food findByName(String name) throws DaoException;
    List<Food> findByType(String type) throws DaoException;


    Food findById(int id) throws DaoException;

    int deleteById(int id) throws DaoException;

    int updatePriceByName(String name, double price) throws DaoException;
    int updateQTYByName(String name, int qty) throws DaoException;
    int updatePriceById(int id, double price) throws DaoException;
    int updateQTYById(int id, int qty) throws DaoException;
}
