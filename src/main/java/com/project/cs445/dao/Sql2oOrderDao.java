package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.Order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import java.util.Date;

/*
 * Created by aqueelmiqdad on 4/10/16.
 */

public class Sql2oOrderDao implements OrderDao {


    private final Sql2o sql2o;

    public Sql2oOrderDao(Sql2o sql2o) {

        this.sql2o = sql2o;
    }

    @Override
    public void add(Order order) throws DaoException {

        String sql = "INSERT INTO ORDERS3(orderNum, ordered_by, amount, itemid, special, surcharge, status, orderDate, deliveryDate, deliveryAddress) " +
                "VALUES(:orderNum, :ordered_by, :amount, :itemid, :special, :surcharge, :status, :orderDate, :deliveryDate, :deliveryAddress)";
        try( Connection con = sql2o.open() ) {

            int id = (int) con.createQuery(sql)
                    .bind(order)
                    .executeUpdate()
                    .getKey();

            order.setId(id);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Insert screwd up in order");
        }
    }

    @Override
    public int setAmount(double amount, int orderNum) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE ORDERS3 SET amount = :amount WHERE orderNum = :orderNum";
            return con.createQuery(sql)
                    .addParameter("orderNum", orderNum)
                    .addParameter("amount", amount)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public List<Order> findAll() throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERS3";
            return con.createQuery(sql)
                    .executeAndFetch(Order.class);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return All Orders Failed");
        }
    }

    @Override
    public List<Order> findByCustomerEmail(String email) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERS3 WHERE ordered_by = :ordered_by";
            return con.createQuery(sql)
                    .addParameter("ordered_by", email)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Cust Email Failed");
        }
    }

    @Override
    public List<Order> findByExactDate(Date date) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            date = df.parse(df.format(date));
            String sql = "SELECT * FROM ORDERS3 WHERE orderDate = :orderDate";
            return con.createQuery(sql)
                    .addParameter("orderDate", date)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findByExactDeliveryDate(Date date, String status) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            date = df.parse(df.format(date));
            String sql = "SELECT * FROM ORDERS3 WHERE deliveryDate = :deliveryDate AND status = :status";
            return con.createQuery(sql)
                    .addParameter("deliveryDate", date)
                    .addParameter("status", status.toLowerCase())
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findByDate(Date date) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            date = df.parse(df.format(date));
            String sql = "SELECT * FROM ORDERS3 WHERE orderDate >= :orderDate";
            return con.createQuery(sql)
                    .addParameter("orderDate", date)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findByDate(Date startDate, Date endDate) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            startDate = df.parse(df.format(startDate));
            endDate = df.parse(df.format(endDate));
            String sql = "SELECT * FROM ORDERS3 WHERE orderDate >= :startDate AND orderDate <= :endDate";
            return con.createQuery(sql)
                    .addParameter("startDate", startDate)
                    .addParameter("endDate", endDate)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findByDeliveryDate(Date startDate) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            startDate = df.parse(df.format(startDate));
            String sql = "SELECT * FROM ORDERS3 WHERE deliveryDate >= :startDate";
            return con.createQuery(sql)
                    .addParameter("startDate", startDate)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findByDeliveryDate(Date startDate, Date endDate) throws DaoException, ParseException {
        try(Connection con = sql2o.open()) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            startDate = df.parse(df.format(startDate));
            endDate = df.parse(df.format(endDate));
            String sql = "SELECT * FROM ORDERS3 WHERE deliveryDate >= :startDate AND deliveryDate <= :endDate";
            return con.createQuery(sql)
                    .addParameter("startDate", startDate)
                    .addParameter("endDate", endDate)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Date Failed");
        }
    }

    @Override
    public List<Order> findOrder(int orderNum) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERS3 WHERE orderNum = :orderNum";
            return con.createQuery(sql)
                    .addParameter("orderNum", orderNum)
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by OrderNum Failed");
        }
    }

    @Override
    public List<Order> findOrderByStatus(String status) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERS3 WHERE status = :status";
            return con.createQuery(sql)
                    .addParameter("status", status.toLowerCase())
                    .executeAndFetch(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by status Failed");
        }
    }

    @Override
    public Order findByOrderId(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERS3 WHERE orderNum = :orderNum";
            return con.createQuery(sql)
                    .addParameter("orderNum", id)
                    .executeAndFetchFirst(Order.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Order by Cust ID Failed");
        }
    }

    @Override
    public int deliverOrder(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE ORDERS3 SET status = :status WHERE orderNum = :orderNum";
            return con.createQuery(sql)
                    .addParameter("orderNum", id)
                    .addParameter("status", "delivered")
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public int cancelOrder(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE ORDERS3 SET status = :status WHERE orderNum = :orderNum";
            return con.createQuery(sql)
                    .addParameter("orderNum", id)
                    .addParameter("status", "cancelled")
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }


}
