package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.OrderItem;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import java.util.*;

/**
 * Created by aqueelmiqdad on 4/15/16.
 */

public class Sql2oItemDao implements ItemDao {

    private final Sql2o sql2o;

    public Sql2oItemDao(Sql2o sql2o) {

        this.sql2o = sql2o;
    }

    public void add(OrderItem item) throws DaoException {
        String sql = "INSERT INTO ORDERITEMS (foodid, qty, price) VALUES (:foodid, :qty, :price)";
        try( Connection con = sql2o.open() ) {

            int id = (int) con.createQuery(sql)
                    .bind(item)
                    .executeUpdate()
                    .getKey();

            item.setId(id);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Insert screwd up in items");
        }
    }

    @Override
    public List<OrderItem> findAll() throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERITEMS";
            return con.createQuery(sql)
                    .executeAndFetch(OrderItem.class);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return All Order Items Failed");
        }
    }

    @Override
    public OrderItem findById(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM ORDERITEMS WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .executeAndFetchFirst(OrderItem.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "FInd By Id in Items Failed");
        }
    }





}
