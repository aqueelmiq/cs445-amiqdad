package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.Food;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */

public class Sql2oFoodDao implements FoodDao {

    private final Sql2o sql2o;

    public Sql2oFoodDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    @Override
    public void addFood(Food food) throws DaoException {
        String sql = "INSERT INTO MENU (name, categories, price_per_person, minimum_order) VALUES (:name, :categories, :price_per_person, :minimum_order)";

        try(Connection con = sql2o.open()) {

            int id = (int) con.createQuery(sql)
                    .bind(food)
                    .executeUpdate()
                    .getKey();

            food.setId(id);

        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Insert screwd up in Food");
        }
    }

    @Override
    public List<Food> findAll() throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM MENU";
            return con.createQuery(sql)
                    .executeAndFetch(Food.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return All Food Failed");
        }
    }

    @Override
    public Food findByName(String name) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM MENU WHERE name = :name";
            return con.createQuery(sql)
                    .addParameter("name", name)
                    .executeAndFetchFirst(Food.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public List<Food> findByType(String categories) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM MENU WHERE categories = :categories";
            return con.createQuery(sql)
                    .addParameter("categories", categories)
                    .executeAndFetch(Food.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public Food findById(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "SELECT * FROM MENU WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .executeAndFetchFirst(Food.class);
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public int deleteById(int id) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "DELETE FROM MENU WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }


    @Override
    public int updatePriceByName(String name, double price_per_person) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE MENU SET price_per_person = :price_per_person WHERE name = :name";
            return con.createQuery(sql)
                    .addParameter("name", name)
                    .addParameter("price_per_person", price_per_person)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public int updateQTYByName(String name, int qty) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE MENU SET minimum_order = :qty WHERE name = :name";
            return con.createQuery(sql)
                    .addParameter("name", name)
                    .addParameter("qty", qty)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }

    @Override
    public int updatePriceById(int id, double price_per_person) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE MENU SET price_per_person = :price_per_person WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .addParameter("price_per_person", price_per_person)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Update price_per_person Food Failed");
        }
    }

    @Override
    public int updateQTYById(int id, int qty) throws DaoException {
        try(Connection con = sql2o.open()) {

            String sql = "UPDATE MENU SET minimum_order = :qty WHERE id = :id";
            return con.createQuery(sql)
                    .addParameter("id", id)
                    .addParameter("qty", qty)
                    .executeUpdate()
                    .getResult();
        } catch (Sql2oException ex) {
            throw new DaoException(ex, "Return Food Failed");
        }
    }


}
