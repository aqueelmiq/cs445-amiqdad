CREATE TABLE IF NOT EXISTS MENU(

    id int PRIMARY KEY auto_increment,
    name VARCHAR,
    categories VARCHAR,
    price_per_person DOUBLE,
    minimum_order INTEGER

);

CREATE TABLE IF NOT EXISTS CUSTOMERS(

    id int PRIMARY KEY auto_increment,
    fName VARCHAR,
    lName VARCHAR,
    phone VARCHAR,
    email VARCHAR

);

CREATE TABLE IF NOT EXISTS ORDERITEMS(

    id int PRIMARY KEY auto_increment,
    foodid int,
    qty int,
    price DOUBLE,
    FOREIGN KEY (foodid) REFERENCES MENU(id)

);

CREATE TABLE IF NOT EXISTS ORDERS3(

    id int PRIMARY KEY auto_increment,
    orderNum int,
    ordered_by VARCHAR,
    itemid int,
    special VARCHAR,
    surcharge DOUBLE,
    status VARCHAR,
    orderDate DATE,
    deliveryDate DATE,
    deliveryAddress VARCHAR,
    amount DOUBLE,
    FOREIGN KEY (ordered_by) REFERENCES CUSTOMERS(email),
    FOREIGN KEY (itemid) REFERENCES ORDERITEMS(id)

);


