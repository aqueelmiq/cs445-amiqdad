Configuration instructions:

Built in IntelliJ Idea
Runnable using standard Java compiler v 1.8

Build and deploy instructions:

Suggested Deploy instructions:
Clone the source from Bitbucket
Import project to IntelliJ
Use IntelliJ to grade build (requires Java SE 1.8)

Standard instructions for Ubuntu:
      
      sudo add-apt-repository ppa:cwchien/gradle
      sudo apt-get update
      sudo apt-get install gradle
Clone the source from Bitbucket :
git clone https://amiqdad@bitbucket.org/amiqdad/cs445-amiqdad.git
cd into the cloned folder
gradle wrapper
      ./gradlew build
./gradlew run

Maven dependencies:

dependencies {
    compile 'org.sql2o:sql2o:1.5.4'
    compile 'com.h2database:h2:1.4.191'
    compile 'com.sparkjava:spark-core:2.3'
    compile 'com.google.code.gson:gson:2.5'
    testCompile group: 'junit', name: 'junit', version: '4.11'
}

Build the project

(Get gradle builder (https://spring.io/guides/gs/gradle/
Use gradle to easily compile the project in ubuntu or other linux)

Testing instructions:

host and port:

http://localhost:8080/


Copyright and licensing instructions:
All code is my original work or based on java open source libraries. There is no further copyright or licensing requirements.

Known bugs:
No significant known bugs

Credits and acknowledgements:

Sql2o Open source Library used to link SQL Database
gson Open source Library by Google to convert Json